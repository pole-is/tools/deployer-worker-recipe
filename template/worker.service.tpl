[Unit]
Description={{ worker.description }}
Requires={{ worker.require_services }}

[Service]
Type=simple
Restart=always
ExecStart={{ worker.command_line }}
WorkingDirectory={{ worker.working_directory }}
User={{ worker.user }}
Group={{ worker.group }}
UMask=0002

[Install]
WantedBy=multi-user.target
