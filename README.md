## irstea/deployer-worker-recipe

"Recettes" pour gérer et déployer un service "worker" avec Deployer.

## Installation

```shell
composer require --dev irstea/deployer-worker-recipe
```

## Utilisation

Il faut inclure les recettes dans le fichier `deploy.php` du projet en utilisant la directive
`require`.

```php
<?php declare(strict_types=1);

namespace Deployer;

// Inclut la recette deployer standard pour Symfony
require 'recipe/symfony.php';

// Les systèmes cibles utilisent Systemd
require __DIR__ . '/vendor/irstea/deployer-worker-recipe/recipe/systemd.php';

// Nous voulons installer un worker symfony/messenger
require __DIR__ . '/vendor/irstea/deployer-worker-recipe/recipe/messenger.php';

// ... snip ! snip ! ...

// Arrête le worker avant de migrer la base de données
before('database:migrate', 'worker:stop');
```

## Commandes

Ce paquet définit des commandes pour deployer dans le namespace `worker:`.

Comme toutes les commandes de deployer, elle prenne le nom de l'environnement de déploiment en premier paramètre.

**Exemple:**

```console
$ vendor/bin/dep worker:status test
➤ Executing task worker:status
deepomics-worker is failed

✔ Ok
```

| Commande | Description | before/after |
| -------  | ----------- | ------------ |
| `worker:start` | Démarre le worker | - |
| `worker:stop` | Arrête le worker ; ne fait rien s'il n'est pas en fonctionnement | - |
| `worker:status` | Affiche le statut du worker | - |
| `worker:restart` | Redémarre le worker | - |
| `worker:service:generate` | Génère la configuration du service | after `deploy:vendors` | - |
| `worker:service:reload` | Regénère la configuration du service puis la recharge | - |
| `worker:reset` | Arrête le worker, met à jour le service, puis le redémarre | after `deploy:symlink`, `rollback` et `deploy:failed` |


## Paramètres

Tous les paramètres commençent par `worker.`. Ils peuvent être écrasés dans votre fichier `deploy.php`
avec `set();`.

**Exemple :**

```php
<?php
// ...
set('worker.require_services', 'postgresql.service elasticsearch.service');
// ...
```

| Paramètres | Description | Valeur par défaut | Exemple |
| ---- | ---- | ---- | ---- |
| **Paramètres communs** |
| `worker.description` | Description du service | `{{ application }} worker` | `monapp worker` |
| `worker.user` | Utilisateur Unix d'éxecution du worker | `{{ http_user }}` | `www-data` |
| `worker.group` | Groupe Unix d'éxecution du worker | Groupe principal de `worker.user` | `www-data` |
| `worker.working_directory` | Chemin de travail du worker | `{{ release_path }}` | `/var/www/monapp/release/25` |
| `worker.command_line` | Ligne de commande complète du worker | `{{ bin/console }} {{ console_options }} --ansi --verbose {{ worker.console_command }}` | `bin/console --no-interaction --env=prod --no-debug --ansi --verbose ...` |
| **Paramètres propres à systemd** |
| `worker.require_services` | Autres services requis par le worker | `postgresql.service` | `postgresql.service elasticsearch.service` |
| `worker.use_sudo` | Utiliser `sudo` pour appeler `systemctl` ? | Oui si on est pas root | `true` |
| `worker.sudo_path` | Chemin exact la commande `sudo` | Déterminé automatiquement | `/usr/bin/sudo` |
| `worker.sudo` | Commande sudo | `{{ worker.sudo_path }} ` ou rien, selon `worker.use_sudo` | `/usr/bin/sudo ` |
| `worker.systemctl_path` | Chemin exact la commande `systemctl` | Déterminé automatiquement | `/bin/systemctl` |
| `worker.systemctl` | Commande systemctl | `{{ worker.sudo }}{{ worker.systemctl_path }}` | `/usr/bin/sudo /bin/systemctl` |
| `worker.service.template` | Chemin du modèle de fichier service | `template/worker.service.tpl` dans le paquet | `/home/user/src/monapp/vendor/irstea/deployer-worker-recipe/template/worker.service.tpl` |
| `worker.service.parsed` | Contenu du modèle après expansion des variables | Dynamique | *Trop grand pour l'afficher ici* |
| `worker.service.name` | Nom du service du worker | `{{ application }}-worker` | `monapp-worker` |
| `worker.service.unit` | Nom de l'unité du worker | `{{ worker.service.name }}.service` | `monapp-worker.service` |
| `worker.service.path` | Chemin vers le fichier de service | `{{ release_path}}/{{ bin_dir }}/{{ worker.service.unit }}` | `/var/www/monapp/release/25/bin/monapp-worker.service` |
| **Paramètres propres à symfony/messenger** |
| `worker.message_limit` | Nombre maximum de message à traiter avant de redémarrer | `50` | `25` |
| `worker.time_limit` | Temps maximum avant de redémarrer (en secondes) | `3600` | `1800` |
| `worker.memory_limit` | Utilisation mémoire nécessitant de redémarrer | `2G` | `512M` |
| `worker.receivers` | Liste des queues à traiter | `async`  | `async_urgent async` |
| `worker.console_command` | Commande Symfony à exécuter | ```messenger:consume --limit={{ worker.message_limit }} --time-limit={{ worker.time_limit }} --memory-limit={{ worker.memory_limit }} {{ worker.receivers }}``` | ```messenger:consume --limit=25 --time-limit=1800 --memory-limit=2G async_urgent async``` |
