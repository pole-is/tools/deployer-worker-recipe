<?php
declare(strict_types=1);
/*
 * irstea/deployer-worker-recipe - Une recette pour deployer un worker.
 * Copyright (C) 2020-2021 IRSTEA
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Deployer;

// Paramètres communs

// Description du worker
set('worker.description', '{{ application }} worker');

// Utilisateur qui doit exécuter le service, par défaut le même que le web
set('worker.user', '{{ http_user }}');

// Groupe de l'utilisateur précédent (déterminé dynamiquement)
set('worker.group', function () { return run('id -gn {{ worker.user }}'); });

// Commande Symfony à exécuter
set('worker.console_command', '# à définir selon la librairie');

// Chemin d'éxecution du worker (celui de la release)
set('worker.working_directory', '{{ release_path }}');

// Ligne de commande complète du worker
set('worker.command_line', '{{ bin/console }} {{ console_options }} --ansi --verbose {{ worker.console_command }}');

// Tâches génériques

desc('Reset the worker');
task('worker:reset', [
    'worker:stop',
    'worker:service:reload',
    'worker:start',
]);

desc('Restart the worker');
task('worker:restart', [
    'worker:stop',
    'worker:start',
]);

// Incrustation dans le flow standard

// Génére le fichier de service après
after('deploy:vendors', 'worker:service:generate');

// Récharge le worker après d'éventuels changements de code/config
after('deploy:symlink', 'worker:reset');
after('rollback', 'worker:reset');
after('deploy:failed', 'worker:reset');
