<?php
declare(strict_types=1);
/*
 * irstea/deployer-worker-recipe - Une recette pour deployer un worker.
 * Copyright (C) 2020-2021 IRSTEA
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Deployer;

require_once __DIR__ . '/common.php';

// Paramètres propres à symfony/messneger

// Nombre maximum de messages à traiter en une exécution
set('worker.message_limit', 50);

// Temps d'exécution maximum
set('worker.time_limit', 3600);

// Limite mémoire à ne pas dépasser
set('worker.memory_limit', '2G');

// Liste de receivers à "consommer"
set('worker.receivers', 'async');

// Commande de la console
set(
    'worker.console_command',
    'messenger:consume ' .
    '--limit={{ worker.message_limit }} ' .
    '--time-limit={{ worker.time_limit }} ' .
    '--memory-limit={{ worker.memory_limit }} ' .
    '{{ worker.receivers }}'
);
