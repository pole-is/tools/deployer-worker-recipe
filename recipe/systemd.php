<?php
declare(strict_types=1);
/*
 * irstea/deployer-worker-recipe - Une recette pour deployer un worker.
 * Copyright (C) 2020-2021 IRSTEA
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Deployer;

use Deployer\Exception\RuntimeException;

require_once __DIR__ . '/common.php';

// Paramètres et défintions pour systemd

// Nom des unités dont le worker a besoin
set('worker.require_services', 'postgresql.service');

// Faut-il utiliser sudo ? (oui si on est pas root)
set('worker.use_sudo', function () {
    return test('test `id -un` != "0"');
});

// Chemin du binaire sudo (déterminé automatiquement)
set('worker.sudo_path', function () {
    return locateBinaryPath('sudo');
});

// Commande sudo (vide si worker.use_sudo est aux, worker.sudo_path sinon)
set('worker.sudo', function () {
    return get('worker.use_sudo') ? (get('worker.sudo_path') . ' ') : '';
});

// Chemin du binaire systemctl (déterminé automatiquement)
set('worker.systemctl_path', function () {
    return locateBinaryPath('systemctl');
});

// Commande systemctl avec sudo intégré (inclus )
set('worker.systemctl', '{{ worker.sudo }}{{ worker.systemctl_path }}');

// Chemin du template du service
set('worker.service.template', \dirname(__DIR__) . '/template/worker.service.tpl');

// Contenu du service avec toutes les variables remplacées
set(
    'worker.service.parsed',
    function (): string {
        $template_path = get('worker.service.template');
        $template = file_get_contents($template_path);
        if ($template === false) {
            throw new \RuntimeException("could not read `$template_path`");
        }

        return parse($template);
    }
);

// Nom (slug) du worker (basé sur le nom de l'application)
set('worker.service.name', '{{ application }}-worker');

// Nom de l'unité systemd
set('worker.service.unit', '{{ worker.service.name }}.service');

// Chemin vers le fichier du service de cette release
set('worker.service.path', '{{ bin_dir }}/{{ worker.service.unit }}');

desc('Generate the worker definition');
task('worker:service:generate', "/bin/cat >'{{ worker.service.path }}' <<'EOF'\n{{ worker.service.parsed }}\nEOF\n");

desc('Reload the worker definition');
task('worker:service:reload', function (): void {
    if (run('{{ worker.systemctl }} is-enabled {{ worker.service.unit }}') === 'enabled') {
        // Recharge toutes les unités
        run('{{ worker.systemctl }} daemon-reload');
    } else {
        // Installe l'unité
        run('{{ worker.systemctl }} enable --force {{ deploy_path }}/current/{{ worker.service.path }}');
    }
});

desc('Start the worker');
task('worker:start', function (): void {
    // Reset le status d'erreur, s'il est défini ; cela permet de forcer un démarrage
    run('{{ worker.systemctl }} reset-failed {{ worker.service.unit }}');
    // Lance le service
    run('{{ worker.systemctl }} start {{ worker.service.unit }}');
});

desc('Stop the worker');
task('worker:stop', function (): void {
    // Ne stoppe le service que s'il est effectivement actif ; cela évite des erreurs inutiles
    if (test('{{ worker.systemctl }} is-active {{ worker.service.unit }}')) {
        run('{{ worker.systemctl }} stop {{ worker.service.unit }}');
    }
});

desc('Show worker status');
task('worker:status', function (): void {
    try {
        $status = run('{{ worker.systemctl }} is-active {{ worker.service.unit }}');
        $color = 'green';
    } catch (RuntimeException $exception) {
        $status = $exception->getOutput();
        $color = 'red';
    }
    writeln("{{ worker.service.name }} is <fg=$color>$status</fg=$color>");
});
